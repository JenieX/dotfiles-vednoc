syntax on

set number
set rnu
set wrap
set ruler
set showmatch
set autoindent
set expandtab
set softtabstop=4
set shiftwidth=4
set smartindent
set smarttab

map <C-J> <C-W>j<C-W>
map <C-K> <C-W>k<C-W>
set wmh=0